from consolemenu.items import FunctionItem

from knapsack import output, run
from consolemenu import ConsoleMenu


def default_run():
    T = 16
    num_of_ops = 6
    ops = {1: "", 2: "", 3: "1", 4: "2,3", 5: "1,2", 6: "4"}
    time = {1: 5, 2: 6, 3: 2, 4: 5, 5: 4, 6: 3}
    workplaces = run(T, ops, time)
    output(workplaces, time)
    input("Press any key to continue")


def custom_run():
    num_of_ops = None
    time = {}
    ops = {}
    while not num_of_ops:
        num_of_ops = int(input("Input number of operations:\n"))
    for j in range(num_of_ops):
        t = None
        while not t:
            t = int(input("Input time of {} operation:\n".format(j + 1)))
        time[j + 1] = t
    for j in range(num_of_ops):
        req_ops = input("Input required operations for {} operation (in '1,2,3' format):\n".format(j + 1))
        ops[j + 1] = req_ops
    T = None
    while not T:
        T = int(input("Input max time for operations in one workplace:\n"))
    workplaces = run(T, ops, time)
    output(workplaces, time)
    input("Press any key to continue")


if __name__ == '__main__':
    menu = ConsoleMenu("Coursework menu", "Input data or use default:")
    default = FunctionItem("Default run", default_run)
    custom = FunctionItem("Custom run", custom_run)
    menu.append_item(default)
    menu.append_item(custom)
    menu.add_exit()
    menu.show()
