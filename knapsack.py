def knap_sack(time, ops, completed_ops, n=6, T=16):
    K = [[set() for x in range(T + 1)] for x in range(n + 1)]
    # Build table K[][] in bottom up manner
    for i in range(n + 1):
        for t in range(T + 1):
            if i == 0 or t == 0:
                K[i][t] = completed_ops
            elif time[i] <= t:
                before_ops = list(map(lambda string: int(string) if string.strip() else 0, ops[i].split(",")))
                completed_before_ops = set(K[i - 1][t - time[i]]).union(set(completed_ops))
                if i not in completed_before_ops \
                        and (before_ops[0] == 0 or set(before_ops).issubset(completed_before_ops)):
                    completed_before_ops.add(i)
                    K[i][t] = completed_before_ops
                else:
                    K[i][t] = K[i - 1][t]
            else:
                K[i][t] = K[i - 1][t]

    return K[n][T], K[n][T].difference(completed_ops)


def run(T, ops, time):
    completed_ops = set()
    workplaces = []
    while len(completed_ops) < len(time):
        completed_ops, on_this_workplace = knap_sack(time, ops, completed_ops, len(ops), T)
        workplaces.append(on_this_workplace)
    return workplaces


def output(workplaces, time):
    print("Sum of workplaces: {}".format(len(workplaces)))
    for i in range(len(workplaces)):
        print("Operations in {} workplace: {}".format(i + 1, workplaces[i]))
        print("\ttime spent: {}".format(sum(time[t] for t in time.keys() if t in workplaces[i])))
